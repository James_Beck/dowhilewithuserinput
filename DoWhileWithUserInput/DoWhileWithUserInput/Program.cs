﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileWithUserInput
{
    class Program
    {
        static void Main(string[] args)
        {
            //Storage of variables
            int i = 0;
            var counter = 20;
            var user = "0";
            int storei = 1;

            //Interacting to get the users name
            Console.WriteLine("Hi there user, what's you name?");
            user = Console.ReadLine();
            Console.WriteLine($"Thanks {user}, I hope you like defining indexes with numbers");
            Console.WriteLine("That is the help that I'm going to need to get from you");
            Console.WriteLine("With that information, you'll be able to define how many lines of repeated text you want");
            Console.WriteLine("Just so you know, the counter is set at 20");
            Console.WriteLine($"So how many is that {user}? (make sure the input is number, not written in text)");
            //Get the number to change the value of integer index
            i = int.Parse(Console.ReadLine());
            Console.WriteLine("Well, whenever you're ready press any key");
            Console.ReadKey();
            Console.Clear();

            do
            {
                if (storei % 10 == 1)
                {
                    Console.WriteLine($"This is the {storei}st iteration of this line");
                    i++;
                    storei++;
                }
                else if (storei % 10 == 2)
                {
                    Console.WriteLine($"This is the {storei}nd iteration of this line");
                    i++;
                    storei++;
                }
                else if (storei % 10 == 3)
                {
                    Console.WriteLine($"this is the {storei}rd iteration of this line");
                    i++;
                    storei++;
                }
                else
                {
                    Console.WriteLine($"this is the {storei}th iteration of this line");
                    i++;
                    storei++;
                }
            } while (i < counter);
            
        }
    }
}
